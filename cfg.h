/*
 * btscanner - Displays the output of Bluetooth scans
 * Copyright (C) 2003 Pentest Limited
 * 
 * Written 2003 by Tim Hurman <timh at pentest.co.uk>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY
 * RIGHTS.  IN NO EVENT SHALL THE COPYRIGHT HOLDER(S) AND AUTHOR(S) BE LIABLE
 * FOR ANY CLAIM, OR ANY SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY
 * DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 * 
 * ALL LIABILITY, INCLUDING LIABILITY FOR INFRINGEMENT OF ANY PATENTS,
 * COPYRIGHTS, TRADEMARKS OR OTHER RIGHTS, RELATING TO USE OF THIS SOFTWARE
 * IS DISCLAIMED.
 */

/*
 * cfg.h: Config options
 */

#ifndef CFG_H
#define CFG_H

#define SF_NAME  0x01
#define SF_HCI   0x02
#define SF_RSSI  0x04
#define SF_LQ    0x08
#define SF_TXPWR 0x10
#define SF_SDP   0x20

struct rangedef
{
	uint8_t sf; /* scan flags */
	uint64_t start;
	uint64_t end;
	char *vulns;
};
typedef struct rangedef rangedef_t;

/* function defs */
int cfg_parsefile(char *);
int cfg_cleanup(void);

const char *cfg_log_filename(void);
const char *cfg_summary_filename(void);
const char *cfg_oui_filename(void);
const char *cfg_store_filename(void);

rangedef_t *cfg_get_range(uint64_t);

#endif /* CFG_H */

